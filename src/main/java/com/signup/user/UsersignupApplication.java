/**
 * 
 */
package com.signup.user;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 
 * @author Z51-70
 *
 */
@SpringBootApplication
@EnableEurekaClient
@EnableConfigurationProperties
@EntityScan(basePackages = { "com.signup.user.model" })
public class UsersignupApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(UsersignupApplication.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
		
	}
}
