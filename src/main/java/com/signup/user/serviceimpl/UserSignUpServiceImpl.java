package com.signup.user.serviceimpl;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.signup.user.model.Role;
import com.signup.user.model.User;
import com.signup.user.repository.UserDao;
import com.signup.user.service.UserSignUpService;

@Component
public class UserSignUpServiceImpl implements UserSignUpService {

	@Autowired
	UserDao userDao;

	@Override
	public void addUser(User user) {
		Role role = new Role();
		role.setName("Buyer");
		Set<Role> roleSet = new HashSet<>();
		roleSet.add(role);
		user.setRole(roleSet);
		userDao.insert(user);

	}

}
