/**
 * 
 */
package com.signup.user.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Z51-70
 *
 */
@Entity
@Table(name = "USER_DETAILS")
public class User implements Serializable {
	
	private static final long serialVersionUID = -8607449210395418230L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "USER_ID")
	private Long id;

	@Column(name = "FIRSTNAME")
	@NotNull
	private String firstName;

	@Column(name = "LASTNAME")
	@NotNull
	private String lastName;

	@Column(name = "EMAIL", unique = true)
	@NotNull
	@Email
	private String email;

	@Column(name = "PASSWORD")
	@NotNull
	@Size(min = 8, message = "Password should have atleast 8 characters")
	private String password;
	
	 @ManyToMany(cascade=CascadeType.ALL)
	 @JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"),
	 inverseJoinColumns = @JoinColumn(name = "role_id")) 
	 private Set<Role> roles;
	 
	private transient String confirmPassword;
	private transient String confirmEmail;

	public User() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	

	public String getConfirmEmail() {
		return confirmEmail;
	}

	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}
	
	

	
	  public Set<Role> getRole() { return roles; }
	  
	  public void setRole(Set<Role> role) { this.roles = role; }
	 

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}
}
