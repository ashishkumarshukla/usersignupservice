package com.signup.user.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "USER_ROLES")
public class Role implements Serializable{
	
	  private static final long serialVersionUID = -3204577533314875544L;
	  
	  @Id
	  
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  
	  @Column(name = "USER_ID") private Long id;
	  
	  @Column(name="Name")
	  
	  @NotNull private String name;
	  
	  @ManyToMany(mappedBy = "roles") 
	  private Set<User> users;
	  
	  public Long getId() { return id; }
	  
	  public void setId(Long id) { this.id = id; }
	  
	  public String getName() { return name; }
	  
	  public void setName(String name) { this.name = name; }
	  
	  public Set<User> getUsers() { return users; }
	  
	  public void setUsers(Set<User> users) { this.users = users; }
	  
	  public Role() {}
	 
	
}
