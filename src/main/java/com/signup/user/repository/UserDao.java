package com.signup.user.repository;

import com.signup.user.model.User;

public interface UserDao {

	public long insert(User user);

}
