/**
 * 
 */
package com.signup.user.service;

import com.signup.user.model.User;

/**
 * @author Z51-70
 *
 */
public interface UserSignUpService {
	public void addUser(User user);

}
