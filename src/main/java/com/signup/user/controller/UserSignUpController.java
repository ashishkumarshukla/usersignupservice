/**
 * 
 */
package com.signup.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.signup.user.model.User;
import com.signup.user.service.UserSignUpService;

/**
 * 
 * @author Z51-70
 *
 */
@RestController
public class UserSignUpController {

	@Autowired
	UserSignUpService userSignUpService;

	@PostMapping(value = "/signup")
	public void createProfile(@RequestBody User user) {
		userSignUpService.addUser(user);

	}
}
