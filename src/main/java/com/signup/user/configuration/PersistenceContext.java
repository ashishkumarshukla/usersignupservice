package com.signup.user.configuration;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class PersistenceContext {

	@Bean(destroyMethod = "close")
	DataSource dataSource() {
		HikariConfig dataSourceConfig = new HikariConfig();
		dataSourceConfig.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		dataSourceConfig.setJdbcUrl("jdbc:oracle:thin:@localhost:1521:ORCLCDB");
		dataSourceConfig.setUsername("dockercontainerpocdb");
		dataSourceConfig.setPassword("dockerpoc");
		dataSourceConfig.setMaximumPoolSize(100);
		dataSourceConfig.addDataSourceProperty("cachePrepStmts", true);
		dataSourceConfig.addDataSourceProperty("prepStmtCacheSize", 250);
		dataSourceConfig.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
		dataSourceConfig.addDataSourceProperty("useServerPrepStmts", true);
		return new HikariDataSource(dataSourceConfig);
	}

	@Bean
	PlatformTransactionManager  transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		//transactionManager.setDataSource(dataSource);
		return transactionManager;
	}

	@Bean
	LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(dataSource);
		entityManagerFactoryBean.setPackagesToScan(new String[] { "com.signup.user.model" });
		entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.dialect", "org.hibernate.dialect.Oracle12cDialect");
		jpaProperties.put("hibernate.hbm2ddl.auto", "update");
		jpaProperties.put("hibernate.naming.physical-strategy",
				"org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl");
		jpaProperties.put("hibernate.show_sql", true);
		jpaProperties.put("hibernate.globally_quoted_identifiers", true);
		entityManagerFactoryBean.setJpaProperties(jpaProperties);
		return entityManagerFactoryBean;
	}

}
