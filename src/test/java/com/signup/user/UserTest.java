package com.signup.user;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import com.signup.user.model.User;

public class UserTest{

	@Test
	public void testEquality() {
		User user = new User();
		user.setId(1L);
		user.setFirstName("Ashish");
		user.setLastName("Shukla");
		user.setEmail("ashishshukla@gmail.com");
		user.setPassword("Ashish1991");
		user.setConfirmPassword("Ashish1991");

		User otherUser = new User();
		otherUser.setId(1L);
		otherUser.setFirstName("Ashish");
		otherUser.setLastName("Shukla");
		otherUser.setEmail("ashishshukla@gmail.com");
		otherUser.setPassword("Ashish1991");
		otherUser.setConfirmPassword("Ashish1991");

		User emptyUser = new User();

		assertEquals(true, user.equals(otherUser));
		assertEquals(true, user.equals(user));
		assertEquals(false, user.equals(emptyUser));
		assertEquals(false, user.equals(null));
		assertEquals(true, user.equals(otherUser));
	}
}
